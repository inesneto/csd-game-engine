const random_line = require("@scrum_project/random");

const get_display_word = word => {
  let display_word= "_ ".repeat(word.length);
  return display_word
}

const replaceAt = (str,index,chr) => {
  if(index > str.length-1) return str;
  return str.substr(0,index) + chr + str.substr(index+1);
}

const startGame = () => {
  let temp_word =  random_line.getRandomWord();
  return {
    status: "RUNNING",
    word:temp_word,
    lives: 5,
    display_word:get_display_word(temp_word), 
    //Colocar "_ " e multiplica-lo pelo size da palavra
    guesses: []
  };
};

const getAllIndexes = (arr, val)  => {
    var indexes = [], i = -1;
    while ((i = arr.indexOf(val, i+1)) != -1){
        indexes.push(i);
    }
    return indexes;
  } 

const guess_word = game_state=>{
  let display_word=get_display_word(game_state.word);

  for (let letter of game_state.word){
    indexes=getAllIndexes(game_state.word,letter)
    if (game_state.guesses.indexOf(letter)>=0) 
    {
      for (let ind of indexes)
        display_word=replaceAt(display_word, (2*ind) ,letter);
    } 
    else display_word
  }
  return display_word
}

const takeGuess = (game_state, guess) => {
  const guess_is_right=game_state.word.indexOf(guess)>=0
  if(game_state.guesses.includes(guess))
{
  return game_state
} 
    if (!guess_is_right)
  {
    if (game_state.lives>1)
    {
    game_state.lives --;
    game_state.guesses.push(guess)
    }
    else if (game_state.lives ==1)
      { 
        game_state.lives --;
        game_state.guesses.push(guess);
        game_state.status="GAMEOVER"
      }
  }
  else if (guess_is_right)
  {
    game_state.guesses.push(guess);
    game_state.display_word=guess_word(game_state)
    if (guess==game_state.word)
      game_state.status="WINNER"
  }
  return game_state
};

module.exports = {
  startGame,
  takeGuess
};
startGame();
