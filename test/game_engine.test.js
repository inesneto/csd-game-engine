const GameEngine = require("../lib/game_engine");

describe("The Game Engine must", () => {
  test("start a new game with a random word", () => {
    let game_state = GameEngine.startGame()
    expect(game_state.lives).toBe(5);
    expect(game_state.display_word.length).toEqual(2*game_state.word.length);
    expect(game_state.word.length).toBeGreaterThan(2)
  });


  test("update the guesses and lives when a wrong guess is given", () => {
  let game_state= GameEngine.startGame();
  game_state.word="fujitsu";

  let new_game_state = GameEngine.takeGuess(game_state,"a");
  expect(new_game_state.lives).toBe(4);
  expect(new_game_state.guesses.length).toBe(1);
  });

test("running out of lives", () => {
    let game_state= GameEngine.startGame();
    game_state.word="fujitsu";
    game_state.lives=1
  
    let new_game_state = GameEngine.takeGuess(game_state,"r");
    expect(new_game_state.lives).toBe(0);
    expect(new_game_state.status).toBe("GAMEOVER");
    });

  test("when a right guess (letter) is given", () => {
          let game_state= GameEngine.startGame();
          game_state.word ="fujitsu"
          game_state.display_word= "_ _ _ _ _ _ _ "
      
          let new_game_state=GameEngine.takeGuess(game_state,"u");
          expect(new_game_state.display_word).toEqual("_ u _ _ _ _ u ");
          expect(new_game_state.lives).toBe(5); 
    });

test("when the right guess is given twice", () => {
          let game_state= GameEngine.startGame();
          game_state.word ="fujitsu"
          game_state.guess=["u"]

      let new_game_state=GameEngine.takeGuess(game_state,"u");
          expect(new_game_state.lives).toBe(5); 
});
});
